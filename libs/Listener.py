"""
  * Connect to a source (a file, an url)
  * Repetitively reload the file (but this can be disabled with polling False)
  * Maintain a pointer for the most recent result
  * Send out updates
"""

from datetime import datetime
from datetime import timedelta
import os
import sys
import time
import json
import urllib2

import threading
import Queue


class Listener(threading.Thread):

    def __init__(self, queue, source=None, filetype="geojson"):
        threading.Thread.__init__(self)
        self.queue = queue

        # self.last_event = time.time()
        self.last_event = 0
        self.start_time = datetime.now()
        self.source = source
        self.filetype = filetype
        self.polling = True
        self.interval = 30
        self.notify = None
        self.closing = False

    def set_source(self, source):
        self.source = source

    def get_source(self):
        return self.source

    def set_tags(self, tags):
        self.tags = tags

    def set_filetype(self, filetype):
        self.filetype = filetype

    def set_interval(self, interval):
        self.interval = interval

    def set_polling(self, polling):
        self.polling = polling

    def get_polling(self):
        return self.polling

    def set_notifier(self, notify):
        self.notify = notify

    def run(self):
        if not self.source:
            # @TODO raise an exception
            return

        if not self.polling:
            self.update()

# If we want to repetatively reload this (the default) then start polling
        while self.polling:
            self.start_time = datetime.now()
            self.stop_time = datetime.now() + timedelta(seconds=self.interval)
            self.update()
            # timedelta = datetime.now() - self.start_time
            t = self.stop_time.strftime("%H:%M:%S")
            sys.stderr.write("Waiting to update %s until %s.\n" %
                            (self.filetype, t))
            # loop for a while until our time is up
            while datetime.now() < self.stop_time:
                if self.closing:
                    break
                try:
                    time.sleep(.1)
                except KeyboardInterrupt:
                    sys.stderr.write("Got KeyboardInterrupt.\n")
                    self.close()
                except:
                    sys.stderr.write("Listener %s caught unexpected error: %s"
                                     % (self.filetype, sys.exc_info()[0]))
                    # @TODO raise an exception

    def close(self):
        self.polling = False
        self.closing = True
        sys.stderr.write("Closed Listener %s.\n" % self.filetype)

    def update(self, polling=True):
        if self.notify:
            self.notify.send(["info"], ["updating", self.filetype])
        if self.filetype == "geojson":
            results = self._update_geojson()

        elif self.filetype == "csv":
            results = self._update_csv()

        if not results:
            return []

        if len(results):
            # sort the results descending (so we get the newest one first)
            from operator import itemgetter
            results = sorted(results, key=itemgetter('time'))
            if not hasattr(self, 'tags') or not "historic" in self.tags:
                results.reverse()

            sys.stderr.write("%d new results in %s.\n"
                             % (len(results), self.filetype))
            i = 1
            for r in results:
                if hasattr(self, 'tags'):
                    r["tags"] = list(self.tags)
                    # append a tag so we know we're done
                    if i == len(results) and "historic" in self.tags:
                        r["tags"].append("last")
                    r["tags"].append(str(i))
                # add the result to the queue for processing
                # elsewhere.
                self.queue.put(r)
                i += 1
        return results

    """
        @TODO inherit the Listener class rather than having csv in here
        @TODO CSV is a little different than GeoJSON usecase. But we should
        make it more similar. CSV reports all results. GeoJSON just the most
        recent.
    """
    def _update_csv(self):
        data = self._get_file()
        if not data:
            return None

        import csv
#        import StringIO

#        buf = StringIO.StringIO(data)
#        buf.readline()

        colheads = None
        reader = csv.reader(data.split("\n"))
        results = []

        for row in reader:
            # .strip() each item in the header row
            row = [x.strip() for x in row]
            #   ignore empty lines
            if not row:
                continue
            #   get the first row as our column headings
            #   (if there is no header row define colheads above)
            if not colheads:
                colheads = row
                continue
            else:
    #   zip the colheads with this row and then make it a dictionary
                row = dict(zip(colheads, row))
    #            from pprint import pprint
    #            pprint(row)
    #   fix up the date
    #   Wednesday, March 16, 2011 10:53:22 UTC
    #   time in geojson is a timestamp
                if row['time']:
                    import dateutil.parser
                    try:
                        row['time'] = time.mktime(
                            dateutil.parser.parse(
                                row['time']).timetuple())
                        row['time'] = int(row['time'])
                    except:
                        sys.stderr.write("Time format for %s not understood."
                                         % row['time'])

    #                dt = datetime.strptime(row['date'],
    #                '%A, %B %d, %Y %H:%M:%S %Z')
    #                row['timestamp'] = time.mktime(dt.timetuple())
    #   create a coords array like geojson
                if row['latitude']:
                    row['coordinates'] = [float(row['longitude']),
                                          float(row['latitude'])]
                    row['magnitude'] = float(row['magnitude'])
#   add the event to our list
            results.append(row)

        return results

    def _update_geojson(self):
        data = self._get_file()
        if not data:
            return None
        results = []
        if data:
            data = json.loads(data)
            if int(data["metadata"]["cacheMaxAge"]) > self.interval:
                self.interval = int(data["metadata"]["cacheMaxAge"])
                sys.stderr.write("Updated interval to %d seconds for %s.\n"
                                 % (self.interval, self.source))

            # collect recent events in our own object format
            if data["type"] == "FeatureCollection":
                events = data["features"]
                for e in events:
                    if e["type"] == "Feature":
                        r = {}
                        r["time"] = int(e["properties"]["time"]) / 1000
                        if r["time"] > self.last_event:
                            self.last_event = r["time"]
                            r["coordinates"] = e["geometry"]["coordinates"]
                            r["magnitude"] = e["properties"]["mag"]
                            if "place" in e["properties"]:
                                r["place"] = e["properties"]["place"]
                            else:
                                r["place"] = "Unknown"
                            results.append(r)
                        else:
                            try:
                                print "%d minutes ago: %f at %s." % (
                                    (time.time() - r["time"]) / 60,
                                    r["place"],
                                    r["magnitude"])
                            except:
                                pass

        return results

    def _get_file(self):
        fh = None
        try:
            if os.path.isfile(self.source):
                sys.stderr.write("Opening as a file.\n")
                fh = open(self.source, "r")
            else:
                fh = urllib2.urlopen(self.source)
        except Exception as e:
            print e
            sys.exc_info()[0]
            sys.stderr.write("Failed to open %s.\n" % self.source)
            return None
        if not fh:
            sys.stderr.write("Something went wrong opening %s.\n"
                             % self.source)
            return None
        sys.stderr.write("Got %s ok.\n" % self.source)
        data = fh.read()
        fh.close()
        return data


if __name__ == "__main__":
    queue = Queue.Queue()

    l = Listener(queue,
                 "http://earthquake.usgs.gov/\
                 earthquakes/feed/geojson/all/hour")
    l.setDaemon(True)
    l.start()

    while True:
        try:
            item = queue.get(True, 1)
            print json.dumps(item, indent=4)
            queue.task_done()
        except Queue.Empty:
#           sys.stderr.write("No new items.\n")
            pass
        time.sleep(1)
