import OSC
import random
import time
import threading
import sys


class MyOSCClient():
    def __init__(self, address='localhost', port=9000,
                 callbacks=(), callback=None):

        self.client = OSC.OSCClient()
        self.client.connect((address, port))
        sys.stderr.write("Connected to %s on port %d.\n" % (address, port))

    def handle_error(self, request, client_address):
        sys.stderr.write("OSC Error handler: %s %s\n" %
                         (request, client_address))

    def send(self, address, message):
        try:
            m = OSC.OSCMessage()
            m.setAddress(address)
            m.extend(list(message))
            self.client.send(m)
        except:
            sys.stderr.write("Error sending to client.\n")

    def close(self):
        self.client.close()
        sys.stderr.write("Closed OSCClient\n")

    def random(self):
        try:
            seed = random.Random()  # need to seed first

            while 1:  # endless loop
                rNum = OSC.OSCMessage()
                rNum.setAddress("/print")
                n = seed.randint(1, 1000)  # get a random num every loop
                rNum.append(n)
                try:
                    self.client.send(rNum)
                    sys.stderr.write("Sent %d.\n" % n)
                except:
                    sys.stderr.write("Could not send message.\n")
                time.sleep(1)  # wait here some secs

        except KeyboardInterrupt:
            sys.stderr.write("Closing OSCClient.\n")
            self.close()

            sys.stderr.write("Done.\n")

    def test(self):
        self.random()

    def _test_handler(self, addr, tags, data, client_address):
        sys.stderr.write("%f %s %s\n" % (time.time(), addr, data[0]))
        return None


def main():
    osc = MyOSCClient()
    osc.test()

if __name__ == '__main__':
    main()
