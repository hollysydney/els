import OSC
from MyOSCClient import MyOSCClient
from OSCMulticlient import OSCMulticlient
from multiprocessing import Process, Event

import sys
import time
import threading
import Queue


class Notifier(threading.Thread):

    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue
        self.running = True
        self.events = {}

        self.address = "localhost"
        self.port = 9000

        sys.stderr.write("Started notifier.\n")

    def run(self):
        cback = None
        self.callbacks = []
        if self.callback:
            cback = self.callback
            self.callbacks = ['set', 'system']
        self.osc = MyOSCClient(address=self.address, port=self.port,
                                  callbacks=self.callbacks, callback=cback)

        self.events["update_variables"] = Event()
        self.events["disconnect"] = Event()
        sys.stderr.write("Connected %s:%s.\n" % (self.address, self.port))

    def close(self):
        self.running = False

        self.events['disconnect'].set()
        sys.stderr.write("Sleeping for 1 second.\n")
        time.sleep(1)
        self.osc.close()
        sys.stderr.write("Closed Notifier.\n")

    def set_port(self, port):
        self.port = int(port)

    def set_address(self, address):
        self.address = address

    def send(self, address, message):
        if address[0] == 'info':
            sys.stderr.write("%s\n" % str(message))

        address = "/%s" % '/'.join(address)
        try:
            self.osc.send(address, message)
            sys.stderr.write("Sent to %s\n" % address)
        except:
            sys.stderr.write("Error creating OSCMessage from %s %s %s\n"
                             % (type(message), message, address))

# callback for osc messages coming in (from pd in this case)
    def callback(self, addr, tags, data, client_address):
        global notify
# strip out leading /
        local_callback = addr[1:]

# any messages that dont have 2 data elements will cause this callback to fail
# messages going out to the wiihandler ( to adjust local variables )
# variables is the data parts - in `set` its variable name, variable value
        [local_variable, local_value] = data
        sys.stderr.write("Received %s %s %s\n"
                         % (local_callback, local_variable, local_value))
        if local_callback == "set":
            sys.stderr.write("Update variables")
            self.variables = [local_variable, local_value]
            self.messenger('update_variables')
            self.osc.send("/info/set", data)
        elif local_callback == "system":
            if local_variable == "quit" and int(local_value) > 0:
                self.osc.send("/info/system", ["shutdown", local_value])
                try:
                    self.close()

                except:
                    sys.stderr.write("Notifier failed to stop.\n")
                    raise KeyboardInterrupt
                sys.exit(0)
        else:
            sys.stderr.write("No call back for %s" % addr)

        # self.variables = None
        return

    def messenger(self, message):
# send messages back up the queue to all the other processes
# TODO - determine message type before sending
# TODO - if its a drop role we need to know which role and team to drop
#   - how do we send a message argument without creating n events?
#   - do we add a variable to an event ? can we get a WiiHandler instance ?

        debug("Tx `%s` message.\n" % message)
        self.events[message].set()
        self.events[message].clear()

    def release_name(self, name, address):
        global names, devices, notify
        sys.stderr.write("Releasing %s.\n" % name)
        names.insert(0, name)
# scan our devices for this address and remove it
        for k, d in devices.iteritems():
#   for d in devices:
#     print "Looking in device %d." % d['id']
            try:
                d['clients'].remove(address)
                sys.stderr.write("Client %s removed from device %d.\n"
                                 % (address, d['id']))
            except:
#       sys.stderr.write("Client %s NOT IN device %d.\n" % (address, d['id']))
                pass

            try:
#       print notify.discoverer.found
                notify.discoverer.found.remove(address)
            except:
                pass
#       sys.stderr.write("Not found in discoverer %s.\n" % address)

    def get_debug(self):
        return debug
