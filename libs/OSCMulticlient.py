"""
The multiclient accepts /subscribe [port] messages ( via its threading server )
and sends all messages to all subscribed servers ( listeners ). The pd patch
`osc-multiclient-server.pd` is set up to send subscribe messages.

Extending this patch we can have listeners request only particular /address
namespaces. eg:
	/redteam
or
	/redteam/1

Messages might be like
	/subscribe 9002 +/redteam


     |      - '[subscribe | listen | sendto | target] <url> [<filter> ...] :  Subscribe remote client/server at <url>,
     |        and/or set message-filters for messages being sent to the subscribed host, with the optional <filter>
     |        arguments. Filters are given as OSC-addresses (or '*') prefixed by a '+' (send matching messages) or
     |        a '-' (don't send matching messages). The wildcard '*', '+*' or '+/*' means 'send all' / 'filter none',
     |        and '-*' or '-/*' means 'send none' / 'filter all' (which is not the same as unsubscribing!)
     |        Reply is an OSCMessage with the (new) subscription; 'target osc://<host>:<port>[<prefix>] [<filter>] [...]'


"""

import OSC
import random
import time
import threading
import sys


class OSCMulticlient():
	def __init__(self, address='127.0.0.1', port=9000, callbacks=(), callback=None):

		self.client = OSC.OSCMultiClient()
		self.server = None
		self.printout = False

		while not self.server:
			try:
				self.addr = address, port
				self.server = OSC.ThreadingOSCServer(self.addr, self.client)
			except:
				sys.stderr.write("Port %d in use.\n" % port)
				port += 1

		sys.stderr.write(str(self.addr) + "\n")

		self.server.addDefaultHandlers()
#		self.server.addMsgHandler("/test", self._test_handler)
#		self.server.addMsgHandler("/info/bout", self._info_handler)
		self.server.addMsgHandler("/subscribe", self._subscribe_handler)
		self.server.handle_error = self.handle_error

		if ( callback ):
			for c in callbacks:
				try:
					sys.stderr.write("Registering callback /%s\n" % c)
					self.server.addMsgHandler("/%s" % c, callback)
				except:
					sys.stderr.write("Could not create callback for %s\n" % c)

#		self.srv_thread = threading.Thread(target=self.server.serve_forever)
		self.serving = True

		self.srv_thread = threading.Thread(target=self.serve)
		self.srv_thread.start()
#		self.serving = True
#		while self.serving:
#			self.server.handle_request


	def handle_error(self, request, client_address):
		sys.stderr.write("OSC Error handler: %s %s\n" % (request, client_address))


	def serve(self):
		sys.stderr.write("OSC serving.\n")
		while self.serving:
			self.server.handle_request()
		sys.stderr.write("OSC server closing.\n")


	def send(self, address, message):
		m = OSC.OSCMessage()
#		print address
		m.setAddress(address)
#			m = OSCMessage(address)
#			m.extend(message)
#		print message
#			self.osc.send(m)
# documentation says list or tuple but only lists work
#		if type(message) == 'tuple':

		m.extend(list(message))
		self.client.send(m)
		if self.printout:
			print "%s %s" % (address, " ".join([str(x) for x in message]))


	def close(self):
		self.serving = False
		time.sleep(1)
		sys.stderr.write("Closing OSCClient\n")
		self.client.close()
		sys.stderr.write("Closing OSCServer\n")
		self.server.close()
		if isinstance(self.srv_thread, threading.Thread) and self.srv_thread.isAlive():
			self.srv_thread.join()

#		print "Done"



	def random(self):
		try:
			seed = random.Random() # need to seed first

			while 1: # endless loop
				rNum= OSC.OSCMessage()
				rNum.setAddress("/print")
				n = seed.randint(1, 1000) # get a random num every loop
				rNum.append(n)
				self.client.send(rNum)
				sys.stderr.write("send %d" % n)
				time.sleep(1) # wait here some secs

		except KeyboardInterrupt:
			sys.stderr.write("Closing OSCClient\n")
			self.client.close()
			sys.stderr.write("Closing OSCServer\n")
			self.server.close()
			if isinstance(self.srv_thread, threading.Thread) and self.srv_thread.isAlive():
				self.srv_thread.join()

			sys.stderr.write("Done\n")


	def _test_handler(self, addr, tags, data, client_address):
		sys.stderr.write("%f %s %s\n" % (time.time(), addr, data[0]))
		return None

#	overrides default /subscribe message
	def _subscribe_handler(self, addr, tags, data, client_address):
		sys.stderr.write("%s %s %s\n" % (addr, client_address, data[0]))
		result = self.server.subscription_handler(addr, tags, data, client_address)
#	result is an OSCMessage
		self.client.send(result)
		return result
	#	return None



def main():
	osc = OSCMulticlient()
	osc.test()

if __name__ == '__main__':
	main()


