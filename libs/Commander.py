"""
Creates an OSC server and listens for messages.
Advertises the service via Bonjour
"""

import select
import sys
import pybonjour
import OSC
import socket
import time

import threading
import Queue
from Notifier import Notifier


class Commander(threading.Thread):

    def __init__(self, callback, port=1337):
        threading.Thread.__init__(self)
        self.callback = callback

# http://mrmr.noisepages.com/publishing-a-performance-using-bonjour/
        self.service_name = "ELS"
        self.regtype = "_mrmrsrvr._udp"
        self.regtype = "_mrmrsrvr_multi._udp"
        self.port = port
        self.service_register = None
        # hostname = socket.gethostname()
        self.address = None
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("gmail.com", 80))
            self.address = s.getsockname()[0]
            s.close()
        except:
            self.address = socket.gethostbyname(socket.gethostname())
        self.serving = False
        self.txtrecord = "patch=Tutorial.mmr"
        self.clients = []

    def run(self):
        self.service_register = pybonjour.DNSServiceRegister(name=self.service_name,
                                             regtype=self.regtype,
                                             port=self.port,
                                             callBack=self.register_callback)

        self.server = OSC.OSCServer((self.address, self.port))
        self.server.addDefaultHandlers()
        self.server.addMsgHandler("/set", self.set_variable_handler)
        self.server.addMsgHandler("/mrmr", self.mrmr_handler)
        # remove the space in the OSC.py library (cdvirtualenv ...)
        self.server.addMsgHandler("/mrmr connect", self.mrmr_connect_handler)
        self.server.addMsgHandler("/mrmr getUI", self.mrmr_ui_handler)
        # anything not trapped above gets handled here
        self.server.addMsgHandler('default', self.mrmr_handler)

        self.server.handle_error = self.handle_error
        sys.stderr.write("Listening for OSC messages on %s:%d.\n" % (self.address, self.port))
        self.serving = True
        self.serve()

    def serve(self):
        sys.stderr.write("OSC serving.\n")
        while self.serving:
            self.server.handle_request()
        sys.stderr.write("OSC server closing.\n")

    def set_variable_handler(self, addr, tags, data, client_address):
        sys.stderr.write("Received message tags %s data %s.\n" % (tags, data))
        key = str(data[0])
        # value could be anything really
        if key == "speed":
            try:
                value = int(data[1])
                self.callback("set", [key, value])
            except:
                sys.stderr.write("Set %s failed.\n" % key)
        elif key == "historic":
            # trigger off the historic playback
            sys.stderr.write("Triggering historic playback.\n")
            self.callback("historic")
            if self.notify:
                self.notify.send(['info'],
                                 ["Control triggering historic playback."])

    def mrmr_handler(self, addr, tags, data, client_address):
        sys.stderr.write("addr: %s\n" % addr)
        sys.stderr.write("tags: %s\n" % tags)
        sys.stderr.write("data: %s\n" % data)
        sys.stderr.write("data: '%s'\n" % data[0])
        paths = addr.split("/")

        if data[0] == "connect":
            sys.stderr.write("Redirecting to mrmr_connect_handler.\n")
            data = [data[1]]
            self.mrmr_connect_handler(addr, tags, data, client_address)
        if paths[1] == "mrmr":
            if paths[2] == "pushbutton":
                button_number = int(paths[3])
                sys.stderr.write("MRMR button %d.\n" % button_number)
                # at the moment just send the historic event
                sys.stderr.write("Callback historic.\n")
                self.callback("historic")

    def mrmr_ui_handler(self, addr, tags, data, client_address):
        sys.stderr.write("Received MRMR UI tags %s data %s.\n"
                         % (tags, data))

    def mrmr_connect_handler(self, addr, tags, data, client_address):
        sys.stderr.write("Received MRMR connect tags %s data %s.\n"
                         % (tags, data))
        if len(data):
            client_address = data[0]
            sys.stderr.write("New client connecting from %s.\n"
                             % client_address)
            #   send the interface out via OSC too
            client = Notifier(None)
            client.set_address(client_address)
            client.set_port(31337)
            client.start()
            self.clients.append(client)
            time.sleep(1.)
            sys.stderr.write("Sending interface.\n")
            # send out the Interface builder commands
            # http://mrmr.noisepages.com/mrmr-interface-protocol/
            # stream = OSC.OSCStreamingClient()
            # stream.connect((client_address, 31337))

            client.send(
                ['mrmrIB'],
                [
                    'mrmr_clear_current'
                ])
            client.send(
                ['mrmrIB'],
                [
#                    'pushbutton', 'nil', ".1", "4", "4", "1", "1", "1", "1", 'test'
                    "pushbutton nil 0.100000 10 18 1 1 1 1 test 2"
                ])
            sys.stderr.write("Sent interface.\n")


    def handle_error(self, arg, arg2):
        # sys.stderr.write(arg)
        pass

    def register_callback(self, service_register, flags, errorCode, name, regtype, domain):
        if errorCode == pybonjour.kDNSServiceErr_NoError:
            print 'Registered service:'
            print '  name    =', name
            print '  regtype =', regtype
            print '  domain  =', domain

    def close(self):
        sys.stderr.write("Closing Commander.\n")
        if self.service_register:
            self.service_register.close()
        for client in self.clients:
            client.close()
        self.serving = False
        sys.stderr.write("Closed Commander.\n")


if __name__ == '__main__':
    queue = Queue.Queue()
    c = Commander(queue)
    c.start()

    try:
        try:
            while True:
                if c.service_register:
                    ready = select.select([c.service_register], [], [])
                    if c.service_register in ready[0]:
                        pybonjour.DNSServiceProcessResult(c.service_register)
        except KeyboardInterrupt:
            pass
    finally:
        c.close()
