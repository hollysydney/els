##Installation

* Install Homebrew
    `ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"`

* Install Python setup tools
    `brew install python`

* Install Pip
    `sudo easy_install pip`

* Install VirtualEnv
* Install VirtualEnv wrapper
    `sudo pip install virtualenv virtualenvwrapper`
* Make a projects directory
    `mkdir ~/python-projects`

* Configure VirtualEnv
(add the following to ~/.bashrc)
    `vi ~/.bashrc`

    `# Python variables`
    `export WORKON_HOME=~/.virtualenvs`
    `export PROJECT_HOME=~/python-projects`
    `source /usr/local/bin/virtualenvwrapper.sh`
    `export PIP_VIRTUALENV_BASE=$WORKON_HOME`
    `export PIP_RESPECT_VIRTUALENV=true`

* Apply the new settings
    `source ~/.bashrc`

* Create an environment
    `mkproject els`

* Clone the repo
    `git clone git@bitbucket.org:laudanum/els.git`

* Install the dependencies
    `cd els && pip install -r requirements.txt`

We have to remove some of the error checking from OSC.py--removing the space in the
        for chk in '*?,[]{}# ':
        for chk in '*?,[]{}#':
so that our "/mrmr connect" address passes

##Updating

* `workon els`
* `git pull`
* `pip install -r requirements.txt`

##Running

Install pd-extended
Open `osc-test-rx.pd`

Run
`python els.py`


##@TODO

* Log events to a file (Logger)
* Store our last timestamp in a config file (on exit and when its updated)
* ~Send out messages (Notifier)~
* Fix the interval to take in to account fetch times (Listener)
* ~Broadcast to localhost (OSCClient)~
