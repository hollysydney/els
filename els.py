#!/usr/bin/python
"""
Features
* Accept OSC servers (what we would call clients)
* Connect to sources
* Broadcast events as they occur
"""

import os
import sys
import time
import pickle
import pprint
import logging
import logging.handlers
import json

from datetime import datetime
from urlparse import urlparse
# import urljoin
from optparse import OptionParser

import threading
import Queue

from libs.OSCMulticlient import OSCMulticlient
from libs.Notifier import Notifier
from libs.Commander import Commander
from libs.Listener import Listener


global options, notify, command, queue
global h, l  # our listeners
global one_day

command = None
one_day = 24 * 60 * 60

options = {}
options['delay'] = 1.
options["debug"] = False
options["historic"] = {}
options['historic']['source'] = "./data/history.csv"
# number of seconds between historic playback
options['historic']['interval'] = 31 * 60  # 31 minutes
options['historic']['original_period'] = 13 * 365.25 * one_day  # 13 years
options['historic']['playback_period'] = 3 * 60 + 33  # 3:33 minutes
options['historic']['ratio'] = options['historic']['playback_period']\
    / options['historic']['original_period']
# Track whether we are in an historic playback phase
options['historic']['running'] = False
# Store the time of the last historic event
options['historic']['last_time'] = False


def _close(code=0):
    global h, l, notify, command
    if notify:
        notify.send(["info"], ["closing"])
        notify.close()
        notify.join()
        sys.stderr.write("Notifier joined.\n")
    if command:
        sys.stderr.write("Commander closing.\n")
        command.close()
        command.join()
        sys.stderr.write("Commander joined.\n")
    l.close()
    l.join()
    sys.stderr.write("Listener joined.\n")
    h.close()
    h.join()
    sys.stderr.write("Historic joined.\n")

    sys.exit(code)


def command_callback(cmd, **kwargs):
    global queue, notify, h, l
    if h and cmd == "historic":
        # clear the queue so historic doesn't compound
        queue.queue.clear()
        # how do we reset the sleep timer?
        notify.send(["info"], ["COMMAND", "Update historic"])
        h.update()
    elif cmd == "set":
        pass

    pass


def calculate_delay(item, last_time):
    global one_day
    # print a date so we can see it
    # the diference in seconds between this event and the previous one
    difference = item["time"] - last_time
    days = float(difference) / one_day
    date = datetime.fromtimestamp(item["time"]).strftime("%Y-%m-%d")
    # the difference in seconds adjusted by the ratio
    seconds = float(difference) * options['historic']['ratio']
    sys.stderr.write("%d days until %s expressed as %d seconds.\n"
                     % (days, date, seconds))
    return seconds


def check_historic(item):
    global options, notify
    # we're running an historic pass
    if "tags" in item and item["tags"][0] == "historic":
        if not options["historic"]["running"]:
            options["historic"]["running"] = True
            if notify:
                notify.send(["speed"], [1])
            else:
                sys.stderr.write("No notify, can't send.\n")
        if options["historic"]["last_time"]:
            # its a future event so recalculate the delay
            if item["time"] > options["historic"]["last_time"]:
                seconds = calculate_delay(item,
                                          options["historic"]["last_time"])
                try:
                    # sleep for a bit
                    time.sleep(seconds)
                except KeyboardInterrupt:
                    _close()
            else:
                # reset last time as we're back at the start
                options["historic"]["last_time"] = None
        # set last_time to the current item
        options["historic"]["last_time"] = item["time"]
        # check to see if we've finished
        if "last" in item["tags"]:
            notify.send(["speed"], [0])
            sys.stderr.write("Historic complete.\n")
            options["historic"]["running"] = False
            options["historic"]["last_time"] = None
        return True
    else:
        if options["historic"]["running"]:
            sys.stderr.write("Historic complete.\n")
            options["historic"]["running"] = False
            # reset last time as we're back at the start
            options["historic"]["last_time"] = None
            if notify:
                notify.send(["speed"], [0])
            else:
                sys.stderr.write("No notify, can't send.\n")


def main():
    global options, notify, h, l, queue, command

    # a queue to handle all the messages between the listeners and notifiers
    queue = Queue.Queue()

    # sends messages to all registered listeners
    notify = Notifier(queue)
    if len(sys.argv) > 1:
        notify.set_port(sys.argv[1])
    notify.start()

    # start a command listener
    command = Commander(command_callback)
    # command.set_notifier(notify)
    command.start()

    # listens for updates from our feeds
    l = Listener(queue, "http://earthquake.usgs.gov/earthquakes/feed/geojson/all/hour")
    l.setDaemon(True)
    l.set_notifier(notify)
    l.start()

    h = Listener(queue, options['historic']['source'], "csv")
    h.set_tags(["historic"])  # an array to append to the address
    h.set_interval(options['historic']['interval'])
    h.set_notifier(notify)
    h.start()

    running = True
    while running:
        try:
            item = queue.get(True, 1)
            address = ["event"]
            if options["debug"] and "tags" in item:
                # this changes the address
                address.extend(item["tags"])
            # this blocks until the proportional time has past if required
            if check_historic(item):
                event_type = "Historic"
                d = "on %s" % datetime.fromtimestamp(item["time"])\
                    .strftime("%Y-%m-%d")

            else:
                event_type = "Current"
                d = "at %s" % datetime.fromtimestamp(item["time"])\
                    .strftime("%H:%M:%S")

            sys.stderr.write("%s event, %s %s in %s.\n" %
                            (event_type, item["magnitude"], d, item["place"]))

            notify.send(address,
                        [
                        item["coordinates"][0],
                        item["coordinates"][1],
                        item["magnitude"]
                        ])
            queue.task_done()
        except Queue.Empty:
            sys.stderr.write("-")
        except KeyboardInterrupt:
            _close()
        time.sleep(.1)


if __name__ == "__main__":
    main()
